------------------------------------------------------------------------------
- ULTIMA DRUPAL THEME
------------------------------------------------------------------------------

Ultimate HTML5 starter theme with robust structure. Unlike most other starter themes, Ultima is a standalone theme which enforces clean structure and does not get into your way. This makes it suitable for any custom design and project of any complexity.

It is not coming with any existing CSS frameworks or styles. Everything is left up to the designer and the specifics of a project.

Ultima is opinionated only when it comes to the file and folder structure. The template.php file is used for including all other function files in the theme, keeping the code well organized and avoiding 1k+ lines long files with functions and overrides.

Features:
- Default implementations of most preprocess hooks (html, node, page etc.)
- Already setup structure and hierarchy, suitable for very complex themes
- Form alter implementation
- Extended body classes
    - Adding vocabulary ID
    - Adding all parts of the current URL; example: url-user url-user-login
    - Adding all roles of the current user; example: user-role-authenticated user-role-administrator
- Separate templates for full and listing (teaser) node views
- Adding page titles to user login, registration and password reset pages
- Skinr and Panels support
- Basic theme settings page for copyright text

Ultima is a starter theme itself and does not rely on any other theme. If you are a beginner or not very familiar with Drupal theming, you should consider using other themes such as Zen or Omega.
http://drupal.org/project/zen
http://drupal.org/project/omega

Ultima theme is created by Topsitemakers
http://www.topsitemakers.com/

------------------------------------------------------------------------------
- CHANGELOG
------------------------------------------------------------------------------

October 2, 2012

- Initial release

*****

By: Topsitemakers
http://www.topsitemakers.com/
