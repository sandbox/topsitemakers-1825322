<?php

/**
 * @file
 * Main theme file
 * Use only as an entry point for other files.
 */

/**
 * Flush theme cache when using "?flush=1" in the URL.
 * Uncomment only during development.
 */
/*
if (isset($_GET['flush'])) {
  drupal_flush_all_caches();
}
*/

/**
 * Custom functions
 */
//

/**
 * Theme functions
 */
require_once dirname(__FILE__) . '/extensions/theme/button.php';
require_once dirname(__FILE__) . '/extensions/theme/links.php';

/**
 * Preprocess functions
 */
require_once dirname(__FILE__) . '/extensions/preprocess/block.php';
require_once dirname(__FILE__) . '/extensions/preprocess/comment-wrapper.php';
require_once dirname(__FILE__) . '/extensions/preprocess/comment.php';
require_once dirname(__FILE__) . '/extensions/preprocess/field.php';
require_once dirname(__FILE__) . '/extensions/preprocess/html.php';
require_once dirname(__FILE__) . '/extensions/preprocess/maintenance-page.php';
require_once dirname(__FILE__) . '/extensions/preprocess/node.php';
require_once dirname(__FILE__) . '/extensions/preprocess/page.php';
require_once dirname(__FILE__) . '/extensions/preprocess/preprocess.php';
require_once dirname(__FILE__) . '/extensions/preprocess/search-result.php';

/**
 * Preprocess functions
 */
//

/**
 * Other hooks
 */
require_once dirname(__FILE__) . '/extensions/hooks/form-alter.php';

/**
 * Form files
 */
//
