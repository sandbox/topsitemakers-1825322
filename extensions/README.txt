------------------------------------------------------------------------------
- /extensions
------------------------------------------------------------------------------

This folder contains all theme functions and implementations of Drupal hooks.
The subfolders are organized according to most used groups of functions:

  /extensions
    /functions     Theme specific functions.
    /hooks         All other hook implementations of Drupal core or modules.
    /preprocess    All preprocess theme functions.
    /process       All process theme functions.
    /theme         All theme implementations.
