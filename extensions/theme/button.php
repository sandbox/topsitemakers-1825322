<?php

/**
 * @file
 * Theme button function.
 */

/**
 * Implements theme_button().
 */
function ultima_button(&$vars) {
  
  $element = $vars['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  // If this is a delete button, add an extra class
  // This can be later on used for special styling (e.g. red buttons).
  if ($element['#value'] == t('Delete') || $element['#id'] == 'edit-delete') {
    $element['#attributes']['class'][] = 'form-button-delete';
  }

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}
