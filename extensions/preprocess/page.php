<?php

/**
 * @file
 * Preprocess page function.
 */
function ultima_preprocess_page(&$vars) {
  
  global $user;

  // Add corresponding page titles to the user pages (login, register, forgot pass).
  if (arg(0)=='user' && !$user->uid) {
    if (!arg(1) || arg(1)=='login') {
      drupal_set_title(t('Log in'));
    }
    elseif (arg(1)=='register') {
      drupal_set_title(t('Create new account'));
    }
    elseif (arg(1)=='password') {
      drupal_set_title(t('Request new password'));
    }
  }

  // Per content type processing
  if (isset($vars['node'])) {
    switch ($vars['node']->type) {
      case 'page':
        break;
    }
  }

}
