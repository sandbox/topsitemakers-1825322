<?php

/**
 * @file
 * Form altering.
 */

/**
 * Implements hook_form_alter().
 */
function ultima_form_alter(&$form, &$form_state, $form_id) {

  /**
   * General overrides
   */
  switch ($form['#id']) {
    case 'comment-form':
      break;
  }

  /**
   * Specific overrides
   */
  switch ($form_id) {

    // Sample implementation for node comment form
    case 'comment_node_type_form':
      break;

    // User login form
    case 'user_login':
      break;

    // User registration form
    case 'user_register_form':
      break;
      
  }

}
