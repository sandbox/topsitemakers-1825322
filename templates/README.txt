------------------------------------------------------------------------------
- /templates
------------------------------------------------------------------------------

Place all template files of your theme here.

It is strongly encouraged to keep the template files in folders named after
that particular module.

For example, "node.tpl.php" belongs to the "node" module and should be placed
into the "node/node.tpl.php" folder.
The same rule should be applied for node templates for specific content types.
