------------------------------------------------------------------------------
- /javascript
------------------------------------------------------------------------------

All theme JS code should be in this folder.

Same logic as in CSS folder applies to overrides - if you need to override any
JS of contributed/core modules, place the file in the 'overrides' subfolder.
