------------------------------------------------------------------------------
- /skins
------------------------------------------------------------------------------

This folder is necessary if you want your theme to integrate with Skinr module.
Existing skinr styles are taken from the example at skinr.org website.
