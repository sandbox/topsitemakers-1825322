Ultimate blank HTML5 starter theme with robust structure.

Unlike most other starter themes, Ultima is a standalone theme which does not get into your way. This makes it suitable for any custom design and project of any complexity.

Ultima is opinionated only when it comes to the file and folder structure. The template.php file is used for including all other function files in the theme, keeping the code well organized and avoiding 1k+ lines long files with functions and overrides.

It is not coming with any existing CSS frameworks or styles. Everything is left up to the designer and the specifics of a project.

<strong>Features:</strong>

<ul>
<li>Default implementations of most preprocess hooks (html, node, page etc.)</li>
<li>Form alter implementation</li>
<li>Extended body classes
   <ul>
     <li>Adding vocabulary ID</li>
     <li>Adding all parts of the current URL; for example: url-user url-user-login</li>
     <li>Adding all roles of the current user; example: user-role-authenticated user-role-administrator</li>
   </ul>
</li>
<li>Separate templates for full and listing (teaser) node views</li>
<li>Adding page titles to user login, registration and password reset pages</li>
<li>Skinr and Panels support</li>
<li>Basic theme settings page for copyright text</li>
</ul>

Ultima is a starter theme itself and does not rely on any other theme. If you are a beginner or not very familiar with Drupal theming, you should consider using other themes such as <a href="http://drupal.org/project/zen">Zen</a> or <a href="http://drupal.org/project/omega">Omega</a>.

Ultima theme is created by Topsitemakers
<a href="http://www.topsitemakers.com/">http://www.topsitemakers.com/</a>